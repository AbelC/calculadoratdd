package com.tdd.calculator;

public class Calculator {

    private Double a;
    private Double b;


    public Calculator(double a, double b) {
        this.a = new Double(a);
       // this.a = a;
        this.b = new Double(b);
        //this.b = b;
    }

    public Double suma() {
        return a+b;
    }

    public Double resta() {
        return a-b;
    }

    public Double multiplicacion() {
        return a*b;
    }

    public Double divicion() {

        if(b != 0 ){
            return a/b;
        }else{
            return new Double(0);
        }

    }
}
