package com.tdd;

import com.tdd.calculator.Calculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void testSuma(){
        Calculator calculator = new Calculator(50,11);
        Double resultado = calculator.suma();

        // delta = 0 > 100% de presicion
        // delta = 1 > 0% de presicion
        assertEquals(61.0, resultado,0);

    }

    @Test
    public void testResta(){
        Calculator calculator = new Calculator(100,50);
        Double resultado = calculator.resta();
        assertEquals(50, resultado,0);
    }

    @Test
    public void testMultiplicacion(){
        Calculator calculator = new Calculator(10,2);
        Double resultado = calculator.multiplicacion();
        assertEquals(20, resultado,0);
    }

    @Test
    public void testDivicion(){
        Calculator calculator = new Calculator(100,2);
        Double resultado = calculator.divicion();

        assertEquals(50, resultado,0);

    }
    @Test
    public void testDivicionConCero(){
        Calculator calculator = new Calculator(80,0);
        Double resultado = calculator.divicion();
        assertEquals(0.0, resultado,0);
    }
}
